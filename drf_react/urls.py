"""drf_react URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView
from product.views import ProductList, product_detail, ProductLocationList, product_location_detail


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^products/(?P<pk>[0-9]+)$', product_detail),
    url(r'^products*$', ProductList.as_view()),
    url(r'^product_details/(?P<pk>[0-9]+)$', product_location_detail),
    url(r'^product_details/*$', ProductLocationList.as_view()),
    url(r'^$', TemplateView.as_view(template_name='index.html')),
]
