from dateutil.parser import parse

from django.shortcuts import render, get_object_or_404, render
from .models import Product, ProductLocation
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializer import ProductSerializer, ProductLocationSerializer
from django.core.files.storage import FileSystemStorage
from rest_framework.decorators import api_view
from rest_framework import status


class ProductList(APIView):
    
    def get(self, request):
        product = Product.objects.all()
        serializer = ProductSerializer(product, many=True)
        return Response(serializer.data)
        
    def post(self, request):
        serializer = ProductSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)            


@api_view(['GET', 'PUT'])
def product_detail(request, pk):
    """
    List all snippets, or create a new snippet.
    """
    try:
        snippet = Product.objects.get(pk=pk)
    except Product.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
        
    if request.method == 'GET':
        serializer = ProductSerializer(snippet)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = ProductSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)        
        
class ProductLocationList(APIView):
    
    def get(self, request):
        product_locations = ProductLocation.objects.all()
        serializer = ProductLocationSerializer(product_locations, many=True)
        return Response(serializer.data)
        
    def post(self, request):
        serializer = ProductLocationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)            

@api_view(['GET', 'PUT'])
def product_location_detail(request, pk):
    """
    List all snippets, or create a new snippet.
    """
    try:
        snippet = ProductLocation.objects.get(pk=pk)
    except Product.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
        
    if request.method == 'GET':
        snippet = ProductLocation.objects.filter(product=pk)
        serializer = ProductLocationSerializer(snippet, many=True)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = ProductLocationSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)        
        