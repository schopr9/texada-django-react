# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductLocation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datetime', models.DateTimeField()),
                ('longitude', models.FloatField()),
                ('lattitude', models.FloatField()),
                ('elevation', models.BigIntegerField()),
            ],
        ),
        migrations.RemoveField(
            model_name='product',
            name='datetime',
        ),
        migrations.RemoveField(
            model_name='product',
            name='elevation',
        ),
        migrations.RemoveField(
            model_name='product',
            name='lattitude',
        ),
        migrations.RemoveField(
            model_name='product',
            name='longitude',
        ),
        migrations.AddField(
            model_name='productlocation',
            name='product',
            field=models.ForeignKey(to='product.Product'),
        ),
    ]
