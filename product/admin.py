from django.contrib import admin

from .models import Product, ProductLocation

admin.site.register(Product)
admin.site.register(ProductLocation)
