from rest_framework import serializers

from .models import Product, ProductLocation

        
class ProductSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Product
        field = '__all__'

class ProductLocationSerializer(serializers.ModelSerializer):
    #products = ProductSerializer(source='product')
    
    class Meta:
        model = ProductLocation
        fields  = '__all__'

