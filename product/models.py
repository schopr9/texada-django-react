from __future__ import unicode_literals

from django.db import models

class Product(models.Model):
    description = models.CharField(max_length=250)

    def __str__(self):
        return self.description


class ProductLocation(models.Model):
    datetime = models.DateTimeField()
    longitude = models.FloatField()
    lattitude = models.FloatField()
    elevation = models.BigIntegerField()
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='products')

