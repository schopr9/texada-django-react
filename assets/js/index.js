var React = require('react')
var ReactDOM = require('react-dom')

var ProductList = require('./component/ProductList').ProductList;

ReactDOM.render(<ProductList url='/products' pollInterval={100000} />, 
    document.getElementById('container'))
