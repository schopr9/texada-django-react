var React = require('react')

var ProductList = React.createClass({
    loadProductsFromServer: function(){
        $.ajax({
            url: this.props.url,
            datatype: 'json',
            cache: false,
            success: function(data) {
                this.setState({data: data});
            }.bind(this)
        })
    },

    getInitialState: function() {
        return {data: []};
    },

    componentDidMount: function() {
        this.loadProductsFromServer();
        setInterval(this.loadProductsFromServer, 
                    this.props.pollInterval)
    }, 
    render: function() {
        if (this.state.data) {
            console.log('DATA!')
            var ProductNodes = this.state.data.map(function(product){
                return <li> {product.description} </li>
            })
        }
        return (
            <div>
                <h1>Hello Texada!</h1>
                <ul>
                    {ProductNodes}
                </ul>
            </div>
        )
    }
})

export {ProductList};